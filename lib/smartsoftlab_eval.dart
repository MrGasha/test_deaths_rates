import 'dart:convert';
import 'dart:io';
import 'package:csv/csv.dart';

import './models/data.dart';

const String fileName = 'lib/resources/time_series_covid19_deaths_US.csv';
void getData() async {
  final input = File(fileName).openRead();
  //Every csv row is converted to a list of values.
  //Unquoted strings looking like numbers (integers and doubles) are by default converted to ints or doubles.

  final myList = await input
      .transform(utf8.decoder)
      .transform(CsvToListConverter())
      .toList();
  final int stateIndex = myList[0].indexOf(
      'Province_State'); //Searching for the index of each column State and Population for easy readability and use

  final int populationIndex = myList[0].indexOf('Population');
  final List<List> formatedList = myList.sublist(
      2, myList.length - 1); //Removing the column titles to iterate over later

  List lastEntry = myList[
      1]; //this is for compare within a foor loop the current row with the previous one

  final List<Data> useFulData =
      []; //This is a List of Data containing the meaningful information to solve the asked questions

  int deathCounter = 0;
  int populationCounter = 0;
  for (var row in formatedList) {
    deathCounter += int.parse(lastEntry.last.toString());
    populationCounter += int.parse(lastEntry[populationIndex].toString());

    if (row[stateIndex] != lastEntry[stateIndex]) {
      //comparing the current row with the prev one
      //bellow is to avoid 0 division error
      final double deathRate =
          populationCounter == 0 ? 0 : deathCounter / populationCounter * 100;
      useFulData.add(
        Data(
          lastEntry[stateIndex],
          deathCounter,
          '${deathRate.toStringAsFixed(3)} %',
          populationCounter,
        ),
      ); //updating the data with the new grouped data

      deathCounter = 0;
      populationCounter = 0;
      //reseting
    }
    lastEntry = row; //updating last entry
  }
  //sort based on death count
  useFulData.sort(((a, b) => a.deathCount.compareTo(b.deathCount)));
  print("""
      El estado con mayor acumulado hasta la ultima fecha es ${useFulData.last.state},
      con un total de ${useFulData.last.deathCount} fallecidos.

      El estado con menor acumulado hasta la ultima fecha es ${useFulData.first.state},
      con un total de ${useFulData.first.deathCount} fallecidos.

      el Porcentaje de muertes y el total de la poblacion por estado:

    """);
  //and sort based on rates
  useFulData
      .sort(((a, b) => a.deathVsPopulation.compareTo(b.deathVsPopulation)));
  for (Data data in useFulData) {
    print(
        'Estado: ${data.state} | Porcentaje de Fallecidos: ${data.deathVsPopulation} | Poblacion Total: ${data.population} \n');
  }

  print("""
    \n El estado mas afectado es aquel cuya relacion entre el total de fallecidos y su poblacion total,
    sea la mayor, en este caso el estado mas afectado fue ${useFulData.last.state},
    con una taza de fallecidos vs Poblacion del ${useFulData.last.deathVsPopulation},
    Este numero representa la cantidad de la población que fallecio.
    si fallecen 2 personas en una poblacion de 4 esta taza representaria el 50%
    en cambio si fallecen 10 personas en una poblacion de 100, esta taza representa el 10%
""");
}
