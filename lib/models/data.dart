class Data {
  final String state;
  final int deathCount;
  final String deathVsPopulation;
  final int population;
  Data(
    this.state,
    this.deathCount,
    this.deathVsPopulation,
    this.population,
  );
}
